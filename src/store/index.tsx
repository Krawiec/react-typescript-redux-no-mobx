export namespace Store{
    export type Counter =  {value : number}
    export type Info = {text: string}

    export type All = {
        counter : Counter,
        info : Info
    }

}