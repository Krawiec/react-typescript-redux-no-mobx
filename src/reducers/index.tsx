import { Action } from '../actions';
import { Store } from '../store';
import { ActionTypes } from '../actions';
import { combineReducers } from 'redux';

export const initialState : Store.All = {
    counter : {value : 0},
    info : {text : "hello"}
}

export function counter(state : Store.Counter = initialState.counter, action: Action) :Store.Counter{
    const {value} = state;
    switch(action.type){
        case ActionTypes.INCREMENET : 
            const newValue = value + action.delta;
            return { ...state, value: newValue };
        case ActionTypes.RESET : 
            return { ...state, value : 0 };
        default:
            return state;
    }
}

export function info(state: Store.Info = initialState.info, action: Action) : Store.Info{
    switch(action.type){
        case ActionTypes.INFO : 
            const newTekst : string = 'added from reducer';
            return { ...state, text : state.text + " " + newTekst };
        default : 
            return state;
    }
}

export interface IStoreState{
    readonly counter: any,
    readonly info: any
}

export const rootReducer = combineReducers<IStoreState>({
    counter, 
    info
})