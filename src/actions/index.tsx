export enum ActionTypes{
    INCREMENET = 'INCREMENET',
    RESET = 'RESET',
    INFO = 'INFO'
}

export type Action = {
    type: ActionTypes.INCREMENET,
    delta : number
} | {
    type : ActionTypes.RESET
} | {
    type: ActionTypes.INFO
}