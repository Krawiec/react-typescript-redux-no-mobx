import { ActionTypes, Action } from './index';
import * as Redux from 'redux';

export const incrementCounter = (delta : number) : Action => ({
    type: ActionTypes.INCREMENET,
    delta
});

export const resetCounter = () : Action  => ({
    type: ActionTypes.RESET
});

export const greetingAction = () : Action => ({
    type: ActionTypes.INFO
});

export const testDispatchAction = () => {
    return function(dispatch: Redux.Dispatch<any>) : Action {
        return dispatch(greetingAction());
    }
};