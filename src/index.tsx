import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { rootReducer, IStoreState } from './reducers';
import logger from 'redux-logger';
import * as Redux from 'redux';
import { Action } from './actions'
import thunk from 'redux-thunk';

const store: Redux.Store<IStoreState> = createStore<IStoreState, Action, any, any>(rootReducer, applyMiddleware(logger, thunk));

store.subscribe(() => {
  console.log(store.getState());
})

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
