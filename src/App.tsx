import * as React from 'react';
import './App.css';
import { Store } from './store';
import * as Redux from 'redux';
import * as counterAction  from './actions/counterActions';
import { connect } from 'react-redux';

interface ConnectedState {
  counter: Store.Counter,
  info : Store.Info
}

interface ConnnectedDispatch{
  increment: (n : number) => void;
  greeting : () => void;
  reset: () => void;
  testDispatch : () => void;  
}

interface OwnProps {
  label: string;
}

interface OwnState{
  test: number
}

class App extends React.Component<ConnectedState & ConnnectedDispatch & OwnProps, OwnState> {
  constructor(props: ConnectedState & ConnnectedDispatch & OwnProps, context: OwnState){
    super(props, context);
    this.state = {
      test: 10
    }
  }

  _onClickIncrement = () => { 
    this.props.increment(1);
  }

  _onClickReste = () => { 
    this.props.reset();
  }

  _onClickGreet = () => { 
    this.props.greeting();
  }

  _onTestClickGreet = () => { 
    this.props.testDispatch();
  }

  render() {
    const { counter, info, label } = this.props;
    const { test } = this.state;

    return (
      <div className="App">
      <p>{label}</p>
      <p>{test}</p>
      <p>counter = {counter.value}</p>
      <p>value = {info.text}</p>
      <button ref="increment" onClick = {this._onClickIncrement}> increment </button>
      <button ref="reset" onClick = {this._onClickReste}> Resset </button>
      <button ref="greet" onClick = {this._onClickGreet}> greeting </button>
      <button ref="greet" onClick = {this._onTestClickGreet}> Greet from reducers </button>
      </div>
    );
  }
}

const mapStateToProps = (state: Store.All, ownProps : OwnProps) => ({
  counter: state.counter,
  info: state.info
})

const mapDispatchToProps = (dispatch: Redux.Dispatch<any>):
ConnnectedDispatch => ({
  increment : (n:number) =>{
    dispatch(counterAction.incrementCounter(n));
  },
  greeting: () => {
    dispatch(counterAction.greetingAction());
  },
  reset : () => {
    dispatch(counterAction.resetCounter());
  },
  testDispatch : () => {
    dispatch(counterAction.testDispatchAction());
  }
})

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(App);
